from twitter_api.twitter_path import Paths
from chrome_driver import ChromeDriver
from datetime import date
import time


def twitter_search(tag, username, password):
    driver = ChromeDriver()
    driver.get('https://twitter.com/')
    login(driver, username, password)
    __search_posts(driver, tag)
    posts_data = __get_posts_data(driver, tag)
    for post in posts_data:
        print(post)
    driver.close_driver()
    return {'data': posts_data}


def login(driver, username, password):
    print('log in to twitter')
    driver.fill_in_text_box(path=Paths.USERNAME_BOX, text=username)
    driver.fill_in_text_box(path=Paths.PASSWORD_BOX, text=password)
    driver.click(path=Paths.LOGIN_BTN)
    time.sleep(7)


def __search_posts(driver, tag):
    time.sleep(1)
    driver.fill_in_text_box(path=Paths.SEARCH_BOX, text=tag)
    time.sleep(2)
    driver.enter_key(path=Paths.SEARCH_BOX)
    driver.click(path=Paths.SEARCH_FILTER)


def __get_shown_posts(driver):
    for i in range(0, 5):
        driver.click(Paths.SEARCH_FILTER)
        driver.page_down()
    posts = driver.get_elements(path=Paths.POST_CONTAINER)
    return posts


def __get_posts_data(driver, tag):
    posts = __get_shown_posts(driver)
    time.sleep(5)
    posts_data = []
    for post in posts:
        posts_data.append({
            'author_name': __get_author_name(post),
            'author_url': __get_author_url(post),
            'author_img': __get_author_img(post),
            'comments': __get_comments_amount(post),
            'reactions': __get_reactions_amount(post),
            'post_url': __get_post_url(post),
            'post_date': __get_post_date(post),
            'post_text': __get_post_text(post),
            'post_media': __get_post_media(post),
            'tag': tag
        })
    return posts_data


def __get_author_name(post):
    try:
        return post.find_element_by_xpath(Paths.POST_AUTHOR_NAME).text
    except:
        return "Could not find authors name"


def __get_author_url(post):
    try:
        return post.find_element_by_xpath(Paths.POST_AUTHOR_URL).get_attribute('href')
    except:
        return "Could not find authors url"


def __get_author_img(post):
    try:
        return post.find_element_by_xpath(Paths.POST_AUTHOR_IMG).get_attribute('src')
    except:
        return "Could not find authors img"


def __get_comments_amount(post):
    try:
        comments_amount = post.find_element_by_xpath(Paths.POST_REPLY_AMOUNT).text
        if 'K' in comments_amount:
            return str(int(float(comments_amount.replace('K', '')) * 1000))
        elif 'M' in comments_amount:
            return str(int(float(comments_amount.replace('M', '')) * 1000000))
        return comments_amount
    except:
        return "Could not find comments amount"


def __get_reactions_amount(post):
    try:
        reactions_amount = post.find_element_by_xpath(Paths.POST_LIKE_AMOUNT).text
        if 'K' in reactions_amount:
            return str(int(float(reactions_amount.replace('K', '')) * 1000))
        elif 'M' in reactions_amount:
            return str(int(float(reactions_amount.replace('M', '')) * 1000000))
        return reactions_amount
    except:
        return "Could not find reactions amount"


def __get_post_url(post):
    try:
        return post.find_element_by_xpath(Paths.POST_URL).get_attribute('href')
    except:
        return "Could not find post url"


def __get_post_date(post):
    try:
        post_date = post.find_element_by_xpath(Paths.POST_DATE).text
        if len(post_date.split('s')) > 1 or len(post_date.split('m')) > 1 or len(post_date.split('h')) > 1:
            return date.today().strftime("%b %d %Y")
        return post_date + date.today().strftime(" %Y")
    except:
        return "JAN, 1 1970"


def __get_post_text(post):
    try:
        text = post.find_element_by_xpath(Paths.POST_TEXT).text
        if '…' in text:
            return text.replace('…', '')
        return text

    except:
        return "Could not find post text"


def __get_post_media(post):
    try:
        return post.find_element_by_xpath(Paths.POST_MEDIA_URL).get_attribute("src")
    except:
        return "Could not find media in post"


if __name__ == '__main__':
    twitter_search(tag='brooklyn99', username='972547711592', password='PkF1fDIbUp')

from chrome_driver import ChromeDriver
from twitter_api.twitter_path import Paths
from twitter_api.twitter_search_posts import login
import time


def tw_source_page_posts(source, username, password):
    driver = ChromeDriver()
    driver.get(source)
    __login(driver, username, password)
    posts_data = __get_posts_data(driver)
    print(posts_data)
    driver.close_driver()
    return {'data': posts_data}


def __login(driver, username, password):
    driver.click(Paths.SOURCE_LOGIN)
    login(driver, username, password)


def __get_posts(driver):
    for i in range(0, 10):
        driver.move_to(Paths.POST_AUTHOR_NAME)
        driver.page_down()
        time.sleep(5)
    posts = driver.get_elements(path=Paths.SOURCE_POST_CONTAINER)
    return posts


def __get_shown_posts(driver):
    for i in range(0, 10):
        driver.move_to(Paths.SOURCE_POSTS_TEXT)
        driver.page_down()
    posts = driver.get_elements(path=Paths.POST_CONTAINER)
    return posts


def __get_posts_data(driver):
    posts = __get_shown_posts(driver)
    time.sleep(5)
    posts_data = []
    for post in posts:
        posts_data.append({
            'post_text': __get_post_text(post)
        })
    return posts_data


def __get_post_text(post):
    try:
        text = post.find_element_by_xpath(Paths.SOURCE_POSTS_TEXT).text
        if '…' in text:
            return text.replace('…', '')
        return text
    except:
        return ''


if __name__ == '__main__':
    get_posts_from_source_page(source=PAGE_URL, username='972547711592', password='PkF1fDIbUp')

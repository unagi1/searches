class Paths:
    #   LOG IN
    USERNAME_BOX = '//input[@type="text"]'
    PASSWORD_BOX = '//input[@type="password"]'
    LOGIN_BTN = '//div[@role="button"]'
    SOURCE_LOGIN = '//a[@role="button" and @href="/login"]'

    #   SEARCH POSTS
    SEARCH_BOX = '//input[@type="text"]'
    SEARCH_FILTER = '//div[@role="presentation"]//span[text()="Top"]'

    #   POST CONTENT
    POST_CONTAINER = '//section//div[not(@*)]//article'
    POST_AUTHOR_NAME = './/div[@data-testid="tweet"]//a//span'
    POST_AUTHOR_URL = './/div[@data-testid="tweet"]//a[@role="link"]'
    POST_AUTHOR_IMG = './/div[@data-testid="tweet"]//a[@role="link"]//img'
    POST_TEXT = './/div[@data-testid="tweet"]//div[@lang]'
    POST_DATE = './/div[@data-testid="tweet"]//time'
    POST_REPLY_AMOUNT = './/div[@data-testid="tweet"]//div[contains(@aria-label,"Reply")]'
    POST_LIKE_AMOUNT = './/div[@data-testid="tweet"]//div[contains(@aria-label,"Like")]'
    POST_MEDIA_URL = './/div[@data-testid="tweet"]//a[@target="_blank"]//img[@alt]'
    POST_URL = './/div[@data-testid="tweet"]//time//parent::a'

    #   SOURCE POSTS
    SOURCE_POSTS_CONTAINER = '//section//div[not(@*)]//article'
    SOURCE_POSTS_TEXT = './/div[@data-testid="tweet"]//div[@lang]'









from facebook_api.facebook_search_posts import facebook_search
from facebook_api.facebook_get_from_source import fb_source_page_posts
from twitter_api.twitter_search_posts import twitter_search
from twitter_api.twitter_get_from_source import tw_source_page_posts
from flask import Flask, request
import json

app = Flask(__name__)
# FB
FB_USERNAME = '972545708263'
FB_PASSWORD = 'bA7gLn7YGQfCoB'
# TW
TW_USERNAME = '972547711592'
TW_PASSWORD = 'PkF1fDIbUp'


@app.route('/fb/search_posts', methods=['POST'])
def fb_search_post():
    content = request.get_json(force=True)
    tag = content['tag']
    print(tag)
    return json.dumps(facebook_search(tag=tag, username=FB_USERNAME, password=FB_PASSWORD))


@app.route('/fb/source_posts', methods=['POST'])
def fb_source_post():
    content = request.get_json(force=True)
    source = content['source']
    print(source)
    return json.dumps(fb_source_page_posts(source=source, username=FB_USERNAME, password=FB_PASSWORD))


@app.route('/tw/search_posts', methods=['POST'])
def tw_search_posts():
    content = request.get_json(force=True)
    tag = content['tag']
    print(tag)
    return json.dumps(twitter_search(tag='covid 19', username=TW_USERNAME, password=TW_PASSWORD))


@app.route('/tw/source_posts', methods=['POST'])
def tw_source_post():
    content = request.get_json(force=True)
    source = content['source']
    print(source)
    return json.dumps(tw_source_page_posts(source=source, username=TW_USERNAME, password=TW_PASSWORD))


if __name__ == '__main__':
    app.run(port='5001')

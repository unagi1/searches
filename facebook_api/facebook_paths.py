import datetime


class Paths:
    # login new
    NEW_USERNAME_BOX = '//form//input[@type="text"]'
    NEW_PASSWORD_BOX = '//form//input[@type="password"]'
    NEW_LOGIN_BTN = '//form//button'

    # login old
    OLD_USERNAME_BOX = '//form//input[@type="email"]'
    OLD_PASSWORD_BOX = '//form[@id="login_form"]//input[@type="password"]'
    OLD_LOGIN_BTN = '//form[@id="login_form"]//input[@type="submit"]'

    # blank white screen
    ALERT = '//div[@role="alertdialog"]//parent::div'
    # search post old ui

    # search post new ui
    SEARCH_BOX = '//input[@aria-label="Search Facebook"]'
    POST_BUTTON = '//div[@role="navigation"]//span[text()="Posts"]'

    # filter posts
    POST_SOURCE_FILTER = '//span[text()="Posts From"]'
    SOURCE_OF_POST = '//span[contains(text(), "Public")]'
    POST_YEAR_FILTER = '//span[contains(text(), "Date")]'
    CURRENT_YEAR = '//span[text()={}]'.format(datetime.datetime.now().year)

    # post content
    POST_CONTAINER = '//div[@role="main"]//div[not(@*)]//div[not(@*)]'
    POST_AUTHOR_NAME = './/a[@role="link"]//span'
    POST_AUTHOR_URL = './/div//span//a[@role="link"]'
    POST_AUTHOR_IMG = './/*[name()="image"]'
    POST_TEXT = './/a[@role="link"]//img//ancestor::span'
    POST_DATE = './/a[@role="link"]//img//ancestor::span'
    POST_COMMENTS_AMOUNT = './/div[@role="button"]//span[contains(text(),"Comment")]'
    POST_REACTIONS_AMOUNT = './/div[@role="button"]//span'
    POST_MEDIA_URL = './/a[@role="link"]//div//img[not(contains(@alt,"Public")) and not(contains(@alt,"Custom"))]'
    POST_URL = './/a[@role="link" and not(span)]'

    #  source posts:
    SOURCE_POST_CONTAINER = '//div[@role="main"]//div[not(@*)]//div[not(@*)]//div[not(@*)]'
    SOURCE_POST_TEXT = './/div[@dir="auto" and not(child::*)]'

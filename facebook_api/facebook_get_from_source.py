from chrome_driver import ChromeDriver
from facebook_api.facebook_paths import Paths
from facebook_api.log_in import log_in
import time


def fb_source_page_posts(source, username, password):
    driver = ChromeDriver()
    driver.get(source)
    log_in(driver, username, password)
    posts_data = __get_posts_data(driver)
    for post in posts_data:
        print(post)
    driver.close_driver()
    return {'data': __filter_posts_data(posts_data)}


def __get_posts_data(driver):
    posts = __get_posts(driver)
    posts_data = []
    for post in posts:
        posts_data.append({
            'post_text': __get_post_text(post)
        })
    return posts_data


def __get_posts(driver):
    for i in range(0, 10):
        driver.move_to(Paths.POST_AUTHOR_NAME)
        driver.page_down()
        time.sleep(5)
    posts = driver.get_elements(path=Paths.SOURCE_POST_CONTAINER)
    return posts


def __get_post_text(post):
    try:
        text = post.find_element_by_xpath(Paths.SOURCE_POST_TEXT).text
        return text
    except:
        return ""


def __filter_posts_data(posts):
    filtered_data = []
    for post in posts:
        if post.get('post_text') != '':
            filtered_data.append(post)
    return filtered_data


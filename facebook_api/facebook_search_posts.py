from facebook_api.facebook_paths import Paths
import time
from datetime import date
from facebook_api import log_in
from chrome_driver import ChromeDriver


def facebook_search(tag, username, password):
    driver = ChromeDriver()
    driver.get('https://www.facebook.com/')
    log_in.log_in(driver, username, password)
    __search_posts(driver, tag)
    __filter_relevant_posts(driver)
    posts_data = __get_posts_data(driver, tag)
    for post in posts_data:
        print(post)
    driver.close_driver()
    return {'data': posts_data}


def __search_posts(driver, tag):
    driver.fill_in_text_box(path=Paths.SEARCH_BOX, text=tag)
    time.sleep(2)
    driver.enter_key(path=Paths.SEARCH_BOX)
    driver.click(path=Paths.POST_BUTTON)


def __filter_relevant_posts(driver):
    # _filter_relevant_posts_by_source
    driver.click(path=Paths.POST_SOURCE_FILTER)
    driver.click(path=Paths.SOURCE_OF_POST)
    time.sleep(2)
    # _filter_relevant_posts_by_year
    driver.click(path=Paths.POST_YEAR_FILTER)
    driver.click(path=Paths.CURRENT_YEAR)
    time.sleep(3)


def __get_posts_data(driver, tag):
    posts = __get_posts(driver)
    time.sleep(5)
    posts_data = []
    for post in posts:
        posts_data.append({
            'author_name': __get_author_name(post),
            'author_url': __get_author_url(post),
            'author_img': __get_author_img(post),
            'comments': __get_comments_amount(post),
            'reactions': __get_reactions_amount(post),
            'post_url': __get_post_url(post),
            'post_date': __get_post_date(post),
            'post_text': __get_post_text(post),
            'post_media': __get_post_media(post),
            'tag': tag
        })
    return posts_data


def __get_posts(driver):
    for i in range(0, 10):
        driver.move_to(Paths.POST_AUTHOR_NAME)
        driver.page_down()
        time.sleep(5)
    posts = driver.get_elements(path=Paths.POST_CONTAINER)
    posts = posts
    return posts


def __get_author_name(post):
    try:
        return post.find_element_by_xpath(Paths.POST_AUTHOR_NAME).text
    except:
        return "Could not find authors name"


def __get_author_url(post):
    try:
        return post.find_element_by_xpath(Paths.POST_AUTHOR_URL).get_attribute('href')
    except:
        return "Could not find authors url"


def __get_author_img(post):
    try:
        return post.find_element_by_xpath(Paths.POST_AUTHOR_IMG).get_attribute('xlink:href')
    except:
        return "Could not find authors img"


def __get_comments_amount(post):
    try:
        comments_amount = post.find_element_by_xpath(Paths.POST_COMMENTS_AMOUNT).text.split()[0]
        if 'K' in comments_amount:
            return str(int(float(comments_amount.replace('K', '')) * 1000))
        elif 'M' in comments_amount:
            return str(int(float(comments_amount.replace('M', '')) * 1000000))
        return comments_amount
    except:
        return "Could not find comments amount"


def __get_reactions_amount(post):
    try:
        reactions_amount = post.find_element_by_xpath(Paths.POST_REACTIONS_AMOUNT).text.split()[0]
        if 'K' in reactions_amount:
            return str(int(float(reactions_amount.replace('K', '')) * 1000))
        elif 'M' in reactions_amount:
            return str(int(float(reactions_amount.replace('M', '')) * 1000000))
        return reactions_amount
    except:
        return "Could not find reactions amount"


def __get_post_url(post):
    try:
        return post.find_element_by_xpath(Paths.POST_URL).get_attribute('href')
    except:
        return "Could not find post url"


def __get_post_date(post):
    try:
        post_date = ", ".join(post.find_element_by_xpath(Paths.POST_DATE).text.split()[0:2])
        if 'min' in post_date or \
                'hr' in post_date or\
                'sec' in post_date or\
                'now' in post_date:
            return date.today().strftime("%b, %d %Y")
        return post_date + date.today().strftime(" %Y")
    except:
        return "JAN, 1 1970"


def __get_post_text(post):
    try:
        text = " ".join(post.find_element_by_xpath(Paths.POST_TEXT).text.split()[4:])
        if '…' in text:
            return text.replace('…', '')
        return text

    except:
        return "Could not find post text"


def __get_post_media(post):
    try:
        return post.find_element_by_xpath(Paths.POST_MEDIA_URL).get_attribute("src")
    except:
        return "Could not find media in post"


if __name__ == '__main__':
    facebook_search(tag='brooklyn99', username='972545708263', password='bA7gLn7YGQfCoB')

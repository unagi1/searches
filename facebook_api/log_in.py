from facebook_api.facebook_paths import Paths
import time


def log_in(driver, username, password):
    try:
        print('try old ui login')
        driver.fill_in_text_box(path=Paths.OLD_USERNAME_BOX, text=username)
        driver.fill_in_text_box(path=Paths.OLD_PASSWORD_BOX, text=password)
        driver.click(path=Paths.OLD_LOGIN_BTN)
    except:
        print('try new ui login')
        driver.fill_in_text_box(path=Paths.NEW_USERNAME_BOX, text=username)
        driver.fill_in_text_box(path=Paths.NEW_PASSWORD_BOX, text=password)
        driver.click(path=Paths.NEW_LOGIN_BTN)
    time.sleep(10)
    print("Log in!")

from selenium import webdriver
import platform
import time
from io_semi_automation import IOSemiAutomation
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys


class ChromeDriver:
    def __init__(self):
        recognized_platform = True
        self.options = webdriver.ChromeOptions()
        # Chrome Options
        self.options.add_argument("--disable-infobars")
        self.options.add_argument("--disable-extensions")
        self.options.add_experimental_option("useAutomationExtension", False)
        self.options.add_experimental_option("excludeSwitches", ["enable-automation"])
        __prefs = {
            "profile.default_content_setting_values.notifications": 2,    # 1 - Allow, 2 - Block
            'credentials_enable_service': False,
            'profile': {'password_manager_enabled': False}
            }
        self.options.add_experimental_option("prefs", __prefs)

        if platform.system() == 'Linux':
            self.executable_path = '/home/gil/Desktop/searches/chromedrivers/chromedriver_linux64/chromedriver'
        elif platform.system() == 'Windows':
            self.executable_path = 'chromedrivers/chromedriver_win32/chromedriver.exe'
        else:
            print("can't detect os")
            recognized_platform = False

        if recognized_platform:
            self.driver = webdriver.Chrome(executable_path=self.executable_path, chrome_options=self.options)
            self.driver.maximize_window()
            IOSemiAutomation.send_keys(text='f11')
            time.sleep(5)

    def get(self, url):
        self.driver.get(url)

    def fill_in_text_box(self, path, text):
        text_box_element = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((
            By.XPATH, path
        )))
        IOSemiAutomation.send_keys(text_box_element, text)

    def click(self, path):
        btn_element = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((
            By.XPATH, path
        )))
        IOSemiAutomation.click_element(btn_element)

    def close_driver(self):
        self.driver.close()

    def escape_key(self, path):
        element = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((
            By.XPATH, path
        )))
        element.send_keys(Keys.ESCAPE)

    def enter_key(self, path):
        element = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((
            By.XPATH, path
        )))
        element.send_keys(Keys.ENTER)

    def get_elements(self, path):
        elements = WebDriverWait(self.driver, 10).until(EC.presence_of_all_elements_located((
            By.XPATH, path
        )))
        return elements

    def times_to_scroll(self, times):
        for times in range(0, times):
            self.scroll_window()
            time.sleep(5)

    def scroll_window(self):
        self.driver.execute_script("window.scrollTo(0, 1080)")

    def move_to(self, path):
        element = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((
            By.XPATH, path
        )))
        IOSemiAutomation.move_to_element(element=element)

    def page_down(self):
        web_body = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((
            By.XPATH, '//body'
        )))
        web_body.send_keys(Keys.PAGE_DOWN)


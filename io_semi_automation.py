from pynput import keyboard, mouse
from random import randint, uniform, random
from pyperclip import copy
from selenium.webdriver.support.select import Select
from math import sqrt, pi, cos, sin
import re
import time


class IOSemiAutomation:
    keyboard_controller = keyboard.Controller()
    mouse_controller = mouse.Controller()

    @staticmethod
    def click_element(element, offset={"x": 0, "y": 0}, is_circular=False):
        IOSemiAutomation.move_to_element(element, offset, is_circular)
        IOSemiAutomation._sleep_human_time(mouse=True)
        IOSemiAutomation.mouse_controller.press(mouse.Button.left)
        IOSemiAutomation.mouse_controller.release(mouse.Button.left)

    @staticmethod
    def move_to_element(element, offset={"x": 0, "y": 0}, is_circular=False):
        click_position = IOSemiAutomation._get_point_to_click(element=element, offset=offset, is_circular=is_circular)
        IOSemiAutomation._smooth_move((click_position['x'], click_position['y']))

    @staticmethod
    def move_to_location(location):
        IOSemiAutomation._smooth_move((location['x'], location['y']))

    @staticmethod
    def close_infobar():
        IOSemiAutomation._smooth_move((20, 20))
        IOSemiAutomation._sleep_human_time(mouse=True)
        IOSemiAutomation.mouse_controller.press(mouse.Button.left)
        IOSemiAutomation.mouse_controller.release(mouse.Button.left)

    @staticmethod
    def send_keys(element=None, text="", is_paste=False):
        if element is not None:
            if element.get_attribute("value") != text:
                IOSemiAutomation.click_element(element)
                if element.get_attribute("value") and element.get_attribute("value").strip() != "":
                    with IOSemiAutomation.keyboard_controller.pressed(keyboard.Key.ctrl):
                        IOSemiAutomation._press_key('a')
                    IOSemiAutomation._press_key(keyboard.Key.backspace)

        if text.lower() in keyboard.Key.__members__ and not is_paste and element is None:
            key_press = keyboard.Key[text.lower()]
            IOSemiAutomation._press_key(key_press)
        elif re.search('^~ ?[a-zA-Z]+ ?\+ ?[a-zA-Z]+$', text) and not is_paste and element is None:
            text = text.replace(' ', '').replace('~', '')
            keys = text.split('+')

            if keys[0].lower() in keyboard.Key.__members__ and \
                    (len(keys[1]) == 1 or keys[1].lower() in keyboard.Key.__members__):
                with IOSemiAutomation.keyboard_controller.pressed(keyboard.Key[keys[0].lower()]):
                    IOSemiAutomation._press_key(keyboard.Key[keys[1].lower()] if
                                                keys[1].lower() in keyboard.Key.__members__ else
                                                keys[1].lower())
        elif not is_paste and IOSemiAutomation._is_in_keyboard(text):
            for letter in text:
                # print("KeyCode:{}, letter:{}".format(keyboard.KeyCode.from_char(letter), letter))
                IOSemiAutomation._sleep_human_time()
                IOSemiAutomation._press_key(letter)
        else:
            IOSemiAutomation._sleep_human_time()
            copy(text)
            IOSemiAutomation._sleep_human_time()
            with IOSemiAutomation.keyboard_controller.pressed(keyboard.Key.ctrl):
                IOSemiAutomation._press_key('v')

    @staticmethod
    def select_value(element, value, is_asc):
        select_element = Select(element)
        text_current_value = select_element.all_selected_options[0].get_attribute("value")
        current_value = int(text_current_value) if text_current_value.isdigit() else text_current_value

        if current_value != value:
            IOSemiAutomation.click_element(element)
            IOSemiAutomation.send_keys(text='f11')

            while current_value != value:
                if (not current_value or current_value == '') or \
                        (value > current_value and is_asc) or \
                        (value < current_value and not is_asc):
                    IOSemiAutomation.send_keys(text="down")
                else:
                    IOSemiAutomation.send_keys(text="up")
                time.sleep(0.1)

                text_current_value = select_element.all_selected_options[0].get_attribute("value")
                current_value = int(text_current_value) if text_current_value.isdigit() else text_current_value

            IOSemiAutomation.send_keys(text='f11')
            return True
        return False

    @staticmethod
    def combobox(element, value):
        select_element = Select(element)
        text_current_value = select_element.all_selected_options[0].get_attribute("value")

        if text_current_value != str(value):
            select_element.select_by_value(value)
            return True
        return False

    @staticmethod
    def scroll(x, y):
        print("scroll - x:  {} y: {}".format(x, y))
        IOSemiAutomation.mouse_controller.scroll(x, y)
    
    @staticmethod
    def _press_key(key):
        IOSemiAutomation.keyboard_controller.press(key)
        IOSemiAutomation.keyboard_controller.release(key)

    @staticmethod
    def _is_in_keyboard(text):
        for character in str(text):
            if not ((character >= 'A' and character <= 'Z') or
                    (character >= 'a' and character <= 'z') or
                    (character >= '0' and character <= '9') or
                    (character in '!@#$%^&*()_+-=`~{}[]:;\'"\\|/?><,. ')):
                return False
        return True

    @staticmethod
    def _get_point_to_click(element=None, element_location=None, element_size=None, offset={"x": 0, "y": 0}, is_circular=False):
        safe_wall = 7

        # Get element required properties
        out_of_element_size = {'left': 0, 'top': 0}

        if element:
            out_of_element_size = IOSemiAutomation._get_out_of_element_size(element)
            element_location = element.location_once_scrolled_into_view or element.location
            element_size = element.size
        else:
            element_location = element_location
            element_size = element_size

        # Get element vertexes
        min_x = int(element_location['x'] + out_of_element_size['left'] + offset['x'])
        max_x = int(min_x + element_size['width'])
        min_y = int(element_location['y'] + out_of_element_size['top'] + offset['y'])
        max_y = int(min_y + element_size['height'])

        if not is_circular:
            # Randomize a point on the element
            click_x = randint(min_x + safe_wall, max_x - safe_wall + 1)
            click_y = randint(min_y + safe_wall, max_y - safe_wall + 1)
        else:
            # Calculate the circle center
            center_x = (min_x + max_x) / 2
            center_y = (min_y + max_y) / 2

            # Calculate the circle radius
            circle_radius = (min(element_size['width'], element_size['height']) - safe_wall * 2) / 2

            # Calculate the point in the element
            r = circle_radius * sqrt(random())
            theta = random() * 2 * pi
            click_x = int(center_x + r * cos(theta))
            click_y = int(center_y + r * sin(theta))

        # Make container fixes if needed
        # container_x = click_x + 10 if os.environ.get('AM_I_IN_CONTAINER', False) else click_x
        # container_y = click_y + 10 if os.environ.get('AM_I_IN_CONTAINER', False) else click_y

        return {
            "x": click_x,
            "y": click_y
        }

    @staticmethod
    def _get_out_of_element_size(element):
        margin_left = IOSemiAutomation._get_css_numeric_property('margin-left', element)
        margin_right = IOSemiAutomation._get_css_numeric_property('margin-right', element)
        margin_top = IOSemiAutomation._get_css_numeric_property('margin-top', element)
        margin_bottom = IOSemiAutomation._get_css_numeric_property('margin-bottom', element)
        border_top = IOSemiAutomation._get_css_numeric_property('border-top-width', element)
        border_right = IOSemiAutomation._get_css_numeric_property('border-right-width', element)
        border_left = IOSemiAutomation._get_css_numeric_property('border-left-width', element)
        border_bottom = IOSemiAutomation._get_css_numeric_property('border-bottom-width', element)

        return {
            "top": margin_top + border_top,
            "bottom": margin_bottom + border_bottom,
            "right": margin_right + border_right,
            "left": margin_left + border_left
        }

    @staticmethod
    def _get_css_numeric_property(property_name, element):
        property_value = element.value_of_css_property(property_name) or '0px'

        return int(re.findall("\d+", property_value)[0])

    @staticmethod
    def _smooth_move(point2):
        total_time = uniform(0.1, 0.3)  # in seconds
        draw_steps = randint(500, 700)  # total times to update cursor
        dt = total_time / draw_steps

        for n in range(draw_steps + 1):
            point1 = IOSemiAutomation.mouse_controller.position
            dx = (point2[0] - point1[0]) / draw_steps
            dy = (point2[1] - point1[1]) / draw_steps
            x = int(point1[0] + dx * n)
            y = int(point1[1] + dy * n)
            IOSemiAutomation.mouse_controller.position = (x, y)
            time.sleep(dt)

        time.sleep(uniform(0.1, 0.5))

    @staticmethod
    def _sleep_human_time(mouse=False):
        if mouse:
            time.sleep(uniform(0.05, 0.1))
        else:
            time.sleep(uniform(0.1, 0.5))
